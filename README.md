# Raspberry Pi

Welcome to my home network.

## Docker

I'm using docker to manage all my services running at home.

### [Portainer](https://github.com/portainer/portainer)

Visualize docker container with a web ui.

### [Pi-hole](https://github.com/pi-hole/pi-hole)

A black hole for Internet advertisements.
