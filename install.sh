#!/bin/bash

## Create environment config file
sudo rm -rf /etc/environment
sudo ln -s /home/goldenson/raspberry-pi/environment /etc/environment

# Create symlink for service
sudo rm /etc/systemd/system/docker-compose-app.service
sudo ln -s /home/goldenson/raspberry-pi/docker-compose-app.service /etc/systemd/system/docker-compose-app.service

# Launch service at boot time
sudo systemctl enable docker-compose-app
sudo systemctl start docker-compose-app

## Check it service is running
sleep 5
systemctl is-active --quiet docker-compose-app && echo "✅ Service is running"
